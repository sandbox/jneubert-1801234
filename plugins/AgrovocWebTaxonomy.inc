<?php

class AgrovocWebTaxonomy extends WebTaxonomy {

  /**
   * Implements WebTaxonomy::autocomplete().
   *
   * Makes use of agrovoc_api, which calls the Agrovoc Web Service.
   * The id value retrieved from the service is transformed to
   * a linked data URI.
   * 
   */
  public function autocomplete($string = '') {
    $matches = array();
    $web_tids = array();

    $language_code = 'en';
    $limit = 10;

    $results = agrovoc_api_simple_search_by_mode2($string, 'starting', ',', true, $limit, $language_code);

    foreach ($results as $result) {
      $term_name = $result['term'];
      $uri = 'http://aims.fao.org/aos/agrovoc/c_' . $result['id'];
      $term_info[$term_name] = array(
        'name' => $term_name,
        'web_tid' => $uri,
      );
    }

    return $term_info;
  }

  /**
   * Implements WebTaxonomy::fetchTerm().
   * 
   * The web_tid of the term is a linked data URI. Therefore, it can be
   * accessed via the ARC2 libary to get the skos:prefLabel properties.
   * 
   */
  public function fetchTerm($term) {
    $web_tid = $term->web_tid[LANGUAGE_NONE][0]['value'];

    $language_code = 'en';

    $term_info = array();
    if (!empty($web_tid)) {

      // get access to the linked data resource
      $res = ARC2::getResource();
      $res->setURI($web_tid);

      // get an array of all skos:prefLabels (with language attributes)
      $labels = $res->getProps('http://www.w3.org/2004/02/skos/core#prefLabel');

      // transform to 
      foreach ($labels as $label) {
        if ($label['lang'] == $language_code) {
          $term_name = $label['value'];
          $term_info[$term_name] = array(
            'name' => $term_name,
            'web_tid' => $web_tid,
          );
        }
      }
    } else {
      // @todo If there is no Web Tid, check by name.
    }
    return array_shift($term_info);
  }
}

